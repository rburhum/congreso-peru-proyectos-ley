import requests
import json
from utils import get_db_connection, HEADERS


# Make the request to the API
url = "https://wb2server.congreso.gob.pe/spley-portal-service/proyecto-ley/lista-con-filtro"
params = {
    "perParId": 2021,
    "perLegId": None,
    "comisionId": None,
    "estadoId": None,
    "congresistaId": None,
    "grupoParlamentarioId": None,
    "proponenteId": None,
    "legislaturaId": None,
    "fecPresentacion": None,
    "pleyNum": None,
    "palabras": None,
    "tipoFirmanteId": None,
    "pageSize": 8000,
    "rowStart": 0
}

response = requests.post(url, headers=HEADERS, data=json.dumps(params))

print(response.content.decode('utf-8'))

# Decode the response
data = json.loads(response.content.decode('utf-8'))

# Connect to the PostgreSQL database
connection = get_db_connection()

# Create a cursor
cursor = connection.cursor()

# Check if the table exists
cursor.execute("SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = 'proyectos_ley');")
table_exists = cursor.fetchone()[0]

# If the table does not exist, create it
if not table_exists:
    cursor.execute("CREATE TABLE proyectos_ley (id serial PRIMARY KEY, perParId integer, pleyNum integer UNIQUE, proyectoLey text UNIQUE, desEstado text, fecPresentacion timestamp, titulo text, desProponente text, autores text);")

# Iterate over the projects
for proyecto in data["data"]["proyectos"]:
    # Check if the project already exists in the database
    cursor.execute("SELECT EXISTS (SELECT 1 FROM proyectos_ley WHERE pleyNum = %s)", (proyecto["pleyNum"],))
    project_exists = cursor.fetchone()[0]

    # If the project exists, update the desEstado field
    if project_exists:
        cursor.execute("UPDATE proyectos_ley SET desEstado = %s WHERE pleyNum = %s", (proyecto["desEstado"], proyecto["pleyNum"]))
    else:
        cursor.execute("INSERT INTO proyectos_ley (perParId, pleyNum, proyectoLey, desEstado, fecPresentacion, titulo, desProponente, autores) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", (proyecto["perParId"], proyecto["pleyNum"], proyecto["proyectoLey"], proyecto["desEstado"], proyecto["fecPresentacion"], proyecto["titulo"], proyecto["desProponente"], proyecto["autores"]))

# Commit the changes to the database
connection.commit()

# Close the cursor and the connection
cursor.close()
connection.close()

