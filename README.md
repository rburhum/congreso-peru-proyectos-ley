# Scraper de Leyes del Congreso Peruano
Este código accesa informacion del Congreso Peruano y graba
todos los proyectos de ley en una base de datos (Postgres)
local.

Si usas este código agradecería un mensaje por Twitter a @rburhum :-)

# Instrucciones

## Setup
1) Crea la base de datos o cambia el script para que se conecte a tu base
de datos
```bash
createuser -s rburhum # o cambia el script para tu usuario
createdb congresoley
```

2) Crea el virtual environment para Python. En este ejemplo usamos virtualenv
```bash
mkvirtualenv congreso
workon congreso
pip install -r requirements.txt
```

## Uso
Son 4 scripts. 

1) El primero extrae todos los proyectos de ley del website del
Congreso peruano y los guarda en la base de datos local

```bash
python leyscraper.py
``````

2) El segundo extrae los _autores_, _coautores_ y _adherentes_ de esas
leyes y los graba en tablas separadas. Adicionalmente genera dos tablas
_nodes_ y _edges_ para visualizar un grafo que representa los autores
y co_autores de un proyecto de ley.

```bash
python autores_scraper.py
```
3) El tercero actualiza las bancadas
```bash
python bancada_scraper.py
```

4) El cuarto exporta todos los datos a un CSV
```bash
python export_data.py
```

![image info](./data/colaboracion.png)

## Ejemplo de datos
[Puedes encontrar un ejemplo de los datos extraídos en ./data](./data)

## Ejemplos de queries

1) Congresistas que tienen leyes publicadas en El Peruano que no son declarativas y de las cuales son autores
```sql
select au.name, count(*) as pl_en_el_peruano 
from proyectos_ley pl 
  join autores au on pl.proyectoley = au.proyectoley  
where desestado like '%El Peruano%' 
and titulo not ilike '%que declara%' 
group by name 
order by pl_en_el_peruano desc;
```
2) Congresistas que no tienen ni una ley que no sea declarativa publicada en el Peruano (de las cuales son autores)
```sql
select name, bancada 
from congresistas 
where name not in ( 
  select au.name 
  from proyectos_ley pl 
    join autores au on pl.proyectoley = au.proyectoley  
  where desestado like '%El Peruano%' 
  and titulo not ilike '%que declara%' 
  group by name
);
```

3) Congresistas que tienen leyes que derogan o simplifican otras leyes
```sql
select au.name, pl.titulo 
from proyectos_ley pl 
  join autores au on pl.proyectoley = au.proyectoley  
where desestado like '%El Peruano%' 
and (titulo ilike '%deroga%' OR titulo ilike '%simplifica%') 
order by name desc
```

4) Congresistas que han hecho leyes declarativas
sobre "día de (e.g. feriado)" publicadas en el peruano
```sql
select au.name, pl.titulo 
from proyectos_ley pl 
  join autores au on pl.proyectoley = au.proyectoley  
where desestado like '%El Peruano%' 
and (titulo ilike '%día%') 
and (titulo ilike '%laborable%' 
    OR titulo ilike '%feriado%') 
order by name;
```