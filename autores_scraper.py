import requests
import json
from utils import get_db_connection, setup_table, HEADERS

def fetch_filters():
    # Make the request to the API
    url = "https://wb2server.congreso.gob.pe/spley-portal-service/periodo-parlamentario/2021/filtros"
    response = requests.get(url, headers=HEADERS)

    return json.loads(response.content.decode('utf-8'))

def search_proyectos_ley(congresistaId=None, tipoFirmanteId=None):
    # Make the request to the API
    url = "https://wb2server.congreso.gob.pe/spley-portal-service/proyecto-ley/lista-con-filtro"
    params = {
        "perParId": 2021,
        "perLegId": None,
        "comisionId": None,
        "estadoId": None,
        "congresistaId": congresistaId,
        "grupoParlamentarioId": None,
        "proponenteId": None,
        "legislaturaId": None,
        "fecPresentacion": None,
        "pleyNum": None,
        "palabras": None,
        "tipoFirmanteId": tipoFirmanteId,
        "pageSize": 8000,
        "rowStart": 0
    }

    response = requests.post(url, headers=HEADERS, data=json.dumps(params))

    return json.loads(response.content.decode('utf-8'))

def tag_proyectos_ley(cursor, table_name, congresista_id, congresista_name, data_pl):
    for proyecto_ley in data_pl['data']['proyectos']:
        cursor.execute("INSERT INTO %s (congresista_id, name, perParId, pleyNum, proyectoley) VALUES (%s, '%s', %s, %s, '%s')" % 
                       (table_name,  congresista_id, congresista_name, proyecto_ley['perParId'], proyecto_ley['pleyNum'], proyecto_ley['proyectoLey']))

def main():
    # Connect to the PostgreSQL database
    connection = get_db_connection()

    setup_table(connection, "congresistas", "id serial PRIMARY KEY, congresista_id integer unique, name text, bancada text")
    setup_table(connection, "grupos_parlamentarios", "id serial PRIMARY KEY, grupo_id integer unique, name text")
    setup_table(connection, "autores", "id serial PRIMARY KEY, congresista_id integer, perParId integer, pleyNum integer, name text, proyectoley text")
    setup_table(connection, "coautores", "id serial PRIMARY KEY, congresista_id integer, perParId integer, pleyNum integer, name text, proyectoley text")
    setup_table(connection, "adherentes", "id serial PRIMARY KEY, congresista_id integer, perParId integer, pleyNum integer, name text, proyectoley text")
    setup_table(connection, "nodes", "id serial PRIMARY KEY, name text, proyectoley_count integer, congresista_id integer")
    setup_table(connection, "edges", "id serial PRIMARY KEY, source integer, target integer, weight integer")

    filters = fetch_filters()

    connection.commit()

    counter = 0

    cursor = connection.cursor()
    for autor in filters['data']['autores']:

        cursor.execute("INSERT INTO congresistas (congresista_id, name) VALUES (%s, %s)", (autor['id'], autor['descripcion']))

        print("[" + str(counter) + "] BUSCANDO Congresista: " + autor['descripcion'] + " AUTOR")
        tag_proyectos_ley(cursor, 'autores', autor['id'], autor['descripcion'], search_proyectos_ley(congresistaId=autor['id'], tipoFirmanteId=1))

        print("[" + str(counter) + "] BUSCANDO Congresista: " + autor['descripcion'] + " CO-AUTOR")
        tag_proyectos_ley(cursor, 'coautores', autor['id'], autor['descripcion'], search_proyectos_ley(congresistaId=autor['id'], tipoFirmanteId=2))

        print("[" + str(counter) + "] BUSCANDO Congresista: " + autor['descripcion'] + " Adherente")
        tag_proyectos_ley(cursor, 'adherentes', autor['id'], autor['descripcion'], search_proyectos_ley(congresistaId=autor['id'], tipoFirmanteId=3))

        counter += 1
        connection.commit()

    for grupo in filters['data']['gruposParlamentarios']:
        print("AGRUPACIÓN: " + grupo['descripcion'])
        cursor.execute("INSERT INTO grupos_parlamentarios (id, grupo_id, name) VALUES (%s, %s, %s)", (grupo['id'], grupo['id'], grupo['descripcion']))

    connection.commit()

    print("Updating nodes")
    cursor.execute("insert into nodes (name, proyectoley_count) select name, count(*) as proyecto_ley_count from autores group by name;")
    cursor.execute("update nodes set congresista_id = congresistas.congresista_id from congresistas where nodes.name = congresistas.name;")

    print("Updating edges")
    # select co.*, au.name as autor_name, au.congresista_id as autor_id from coautores co join autores au on co.proyectoley = au.proyectoley;
    cursor.execute("insert into nodes (name, proyectoley_count) select name, count(*) as proyecto_ley_count from autores group by name;")
    cursor.execute("insert into edges (source, target, weight) select co.congresista_id as source, au.congresista_id as target, count(au.congresista_id) as weight from coautores co join autores au on co.proyectoley = au.proyectoley group by source, target order by source, target;")
    
    connection.commit()

if __name__ == "__main__":
    main()