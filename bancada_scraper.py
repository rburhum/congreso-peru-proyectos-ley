import requests
from bs4 import BeautifulSoup
from utils import get_db_connection, HEADERS, remove_accents

def cleanup_name_mess(name):
    """
    Resulta que en la página web de congreso.gob.pe hay algunos nombres
    que estan digititalizados disntintos a como estan en la API
    así que llamamos esta funcion para limpiar esos casos
    """
    if name == "Bustamante Donayre Ernesto":
        return "Bustamante Donayre Carlos Ernesto"

    return name

def scrape_congresistas():
    """Scrapes the names of all the congresistas from the table."""

    url = "https://www.congreso.gob.pe/pleno/congresistas/"
    
    response = requests.get(url, headers=HEADERS)
    soup = BeautifulSoup(response.text, "html.parser")

    table = soup.find("table", class_="congresistas")
    rows = table.find_all("tr")

    congresistas = []
    for row in rows:
        congresista = {}

        cells = row.find_all("a", class_="conginfo")
        if cells:
            congresista['name'] = cleanup_name_mess(cells[0].text)
            cells = row.find_all("span", class_="partidolist")
            congresista['partido'] = cells[0].text
            congresistas.append(congresista)

    return congresistas

def update_bancadas(congresistas):
    """Updates the bancada of each congresista in the database."""

    connection = get_db_connection()
    cursor = connection.cursor()

    for congresista in congresistas:
        
        # lamentablemente el nombre de los congresistas en la página web no coincide en casing con el de la API,
        # ni con los acentos, por lo que vamos a usar el valor del API para actualizar la tabla masajeando ambos
        # para evitar ambos problemas
        sql = """
            UPDATE congresistas 
            SET bancada = b.name
            FROM (
                SELECT name
                FROM grupos_parlamentarios
                WHERE translate(name, 'áéíóúüñÁÉÍÓÚÑ', 'aeiouunAEIOUN') ilike '%{}%'
            ) AS b
            WHERE translate(replace(congresistas.name, ',' , ''), 'áéíóúüñÁÉÍÓÚÑ', 'aeiouunAEIOUN') ilike '%{}%'
        """.format(remove_accents(congresista['partido']), remove_accents(congresista['name']))
        cursor.execute(sql)

        if cursor.rowcount == 0:
            print("No se encontró bancada para", congresista['name'], '-', congresista['partido'])
            print(sql)

    connection.commit()
    connection.close()

if __name__ == "__main__":
    congresistas = scrape_congresistas()
    update_bancadas(congresistas)