import os
from utils import get_db_connection

def main():
    # Connect to the PostgreSQL database
    connection = get_db_connection()
    cursor = connection.cursor()

    current_working_directory = os.getcwd()
    export_folder = os.path.abspath(os.path.join(current_working_directory, 'data'))

    # export data
    print("Exporting data")
    cursor.execute("copy congresistas to '%s/congresistas.csv' csv header;" %(export_folder,))
    cursor.execute("copy grupos_parlamentarios to '%s/grupos_parlamentarios.csv' csv header;" %(export_folder,))
    cursor.execute("copy autores to '%s/autores.csv' csv header;" %(export_folder,))
    cursor.execute("copy coautores to '%s/coautores.csv' csv header;" %(export_folder,))
    cursor.execute("copy adherentes to '%s/adherentes.csv' csv header;" %(export_folder,))
    cursor.execute("copy nodes to '%s/nodes.csv' csv header;" %(export_folder,))
    cursor.execute("copy edges to '%s/edges.csv' csv header;" %(export_folder,))
    cursor.execute("copy (SELECT NOW()) to '%s/fecha_de_export.txt' csv;" %(export_folder,))

    # Close the cursor and the connection
    cursor.close()
    connection.close()

if __name__ == "__main__":
    main()