import psycopg2
import unicodedata

def get_db_connection():
    # Connect to the PostgreSQL database
    return psycopg2.connect(
        host="",
        port=5432,
        database="congresoley",
        user="rburhum",
    )

def setup_table(connection, table_name, column_names):

    # Create a cursor
    cursor = connection.cursor()

    # Check if the table exists
    cursor.execute("SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = %s);", (table_name,))
    table_exists = cursor.fetchone()[0]

    # If the table does not exist, create it
    if not table_exists:
        cursor.execute("CREATE TABLE %s (%s)" % (table_name, column_names))
    else:
        cursor.execute("TRUNCATE TABLE %s;" %(table_name,))
    
    cursor.close()

HEADERS = {
        "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0",
        "Accept": "application/json, text/plain, */*",
        "Accept-Language": "en-US,en;q=0.7,es-PE;q=0.3",
        "Accept-Encoding": "gzip, deflate, br",
        "Content-Type": "application/json"
}

def remove_accents(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    return u"".join([c for c in nfkd_form if not unicodedata.combining(c)])